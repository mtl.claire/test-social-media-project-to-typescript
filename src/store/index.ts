import { createStore, action, thunk } from "easy-peasy";
import { StoreModel } from "../model";

export const store = createStore<StoreModel>({
	posts: [],
	photos: [],
	users: [],
	setPosts: action((state, payload) => {
		return {
			...state,
			posts: [...payload]
		};
	}),
	setPhotos: action((state, payload) => {
		return {
			...state,
			photos: [...payload]
		};
	}),
	setUsers: action((state, payload) => {
		return {
			...state,
			users: [...payload]
		};
	}),
	fetchAllPosts: thunk(async (actions) => {
		const res = await fetch("https://jsonplaceholder.typicode.com/posts");
		const posts = await res.json();
		await actions.setPosts(posts);
	}),
	fetchAllPhotos: thunk(async (actions) => {
		const res = await fetch("https://picsum.photos/v2/list");
		const photos = await res.json();
		await actions.setPhotos(photos);
	}),
	fetchAllUsers: thunk(async (actions) => {
		const res = await fetch("https://jsonplaceholder.typicode.com/users");
		const users = await res.json();
		await actions.setUsers(users);
	}),
	updatePostThunk: thunk(async (actions, payload) => {
		const requestUrl = 'https://jsonplaceholder.typicode.com/posts/' + payload.id; 
		const res = await fetch(requestUrl, {
			method: 'PUT',
			body: JSON.stringify({
				id: payload.id,
				title: payload.title,
				body: payload.body,
				userId: payload.userId,
			}),
			headers: {
			  'Content-type': 'application/json; charset=UTF-8',
			},
		  });
		const currentPost = await res.json();
		await actions.updatePost(currentPost);
	}),
	updatePost: action((state, payload) => {
		return {
			...state,
			posts:  state.posts.map((p) =>
				p.id === payload.id ? { ...p, ...payload } : { ...p }
			)
		};
	}),
	creatPostThunk: thunk(async (actions, payload) => {
		const res = await fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			body: JSON.stringify({
			  title: payload.title,
			  body: payload.body,
			  userId: 1,
			}),
			headers: {
			  'Content-type': 'application/json; charset=UTF-8',
			},
		  });
		const newPost = await res.json();
		await actions.createPost(newPost);
	}),
	createPost: action((state, payload) => {
		return {
			...state,
			posts: [...state.posts, payload] //BUG: mass the post order and edit dialog
		};
	}),
	deletePostThunk: thunk(async (actions, payload) => {
		const res = await fetch("https://jsonplaceholder.typicode.com/posts");
		const posts = await res.json();
		await actions.setPosts(posts);
	}),
	postDialog: {
		isOpen: false,
		data: null
	},
	setDialog: action((state, payload) => {
		console.log("setDialog payload");
		console.log(payload);

		return {
			...state,
			postDialog: {
				isOpen: payload.isOpen,
				data: payload.data
			}
		};
	}),
	currentLanguage: "zh",
	setCurrentLanguage: action((state, payload) => {
		return {
			...state,
			currentLanguage: payload
		};
	}),
});
