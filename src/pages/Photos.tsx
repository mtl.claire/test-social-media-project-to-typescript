import React from "react";
import { useEffect } from "react";
import { Actions } from 'easy-peasy';
import { makeStyles } from "@material-ui/core/styles";
import { StoreModel } from "../model";
import { useStoreState, useStoreActions } from "../store/hooks";
import { PhotoItem } from "../components/PhotoItem";
import { Grid } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: "0"
	},
	wrapper: {
		padding: "10px"
	}
}));

export const PhotosContainer = () => {

	const fetchAllPhotos = useStoreActions(
		(actions: Actions<StoreModel>) => actions.fetchAllPhotos
	);
	
	useEffect(() => {
		fetchAllPhotos();
	}, []);

	const classes = useStyles();
	const photos = useStoreState((state) => state.photos);
	const renderedListItems = photos.map((item, index) => {
		return <PhotoItem key={index} {...item}/>;
	});
	
	return (
		<div className={classes.root}>
			<Grid container spacing={3}>
				{ renderedListItems } 
			</Grid>
		</div>
	);
};