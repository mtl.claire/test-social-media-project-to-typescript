import React from "react";
import { useEffect } from "react";
import List from '@material-ui/core/List';
import { Actions } from 'easy-peasy';
import { StoreModel } from "../model";
import { useStoreState, useStoreActions } from "../store/hooks";
import { UserItem } from "../components/UserItem"

export const UsersContainer = () => {
	const fetchAllUsers = useStoreActions(
		(actions: Actions<StoreModel>) => actions.fetchAllUsers
	);
	
	useEffect(() => {
		fetchAllUsers();
	}, []);
	return (
		<div>
			<h2>Call Friends</h2>
			<UserListComponent />
		</div>
	);
};

const UserListComponent = () => {
	const users = useStoreState((state) => state.users);
	const renderedListItems = users.map((item, index) => {
		return (
			<UserItem key={index} {...item} />
		)
	});

	return (
		<div id="user-container">
			<List>
				{renderedListItems}
			</List>
		</div>
	);
};
