import { useEffect } from "react";
import { Actions } from 'easy-peasy';
import { makeStyles } from "@material-ui/core/styles";
import { StoreModel } from "../model";
import { useStoreState, useStoreActions } from "../store/hooks";
import { PostCardItem } from "../components/PostItem";
import { PostDialog } from "../components/PostDialog";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { Grid } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	root: {
		margin: "0"
	},
	fabButton: {
		position: 'fixed',
		zIndex: 1,
		bottom: 70,
		right: 20,
		color: "#fff",
		backgroundColor: "#303f9f"
	},
}));

export const PostsContainer = () => {
	const classes = useStyles();
	const posts = useStoreState((state) => state.posts);
	const setDialog = useStoreActions(
		(actions: Actions<StoreModel>) => actions.setDialog
	);
	const handleClickOpen = () => {
		setDialog({
			isOpen: true,
			data: null
		});
	};
	const fetchAllPosts = useStoreActions(
		(actions: Actions<StoreModel>) => actions.fetchAllPosts
	);
	useEffect(() => {
		fetchAllPosts();
	},[]);

	return (
		<div id="home-container">
			<div className={classes.root}>
				<Grid container spacing={3}>
					{posts.map((post, index) => (
						<PostCardItem key={index} {...post}/>
					))}
				</Grid>
			</div>
			<PostDialog />
			<Fab aria-label="addPost" className={classes.fabButton} onClick={handleClickOpen}>
				<AddIcon />
			</Fab>
		</div>
	);
};