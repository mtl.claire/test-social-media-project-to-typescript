import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Card, CardHeader, CardMedia, CardContent, CardActions } from "@material-ui/core";
import { Collapse, Avatar, IconButton, Typography} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { StoreModel } from "../model";
import { PostInterface } from "../model/posts";
import { useStoreActions } from "../store/hooks";
import { Actions } from 'easy-peasy';

const useStyles = makeStyles((theme) => ({
	root: {
		padding: "10px"
	},
	media: {
		height: 0,
		paddingTop: "56.25%" // 16:9
	},
	title: {
		flexGrow: 1,
		textAlign: "center"
	  },
	expand: {
		transform: "rotate(0deg)",
		marginLeft: "auto",
		transition: theme.transitions.create("transform", {
			duration: theme.transitions.duration.shortest
		})
	},
	expandOpen: {
		transform: "rotate(180deg)"
	},
	editContainer: {
		padding: "70px 10px"
	},
	textField: {
		width: "calc(100% - 32px)",
		padding: "0 16px"
	},
	appBar: {

	},
	avatar: {
		backgroundColor: "#f50057"
	}
}));

export const PostCardItem = (item:PostInterface) => {
	const classes = useStyles();

	const [expanded, setExpanded] = React.useState(false);
	const handleExpandClick = () => {
		setExpanded(!expanded);
	};

	const setDialog = useStoreActions(
		(actions: Actions<StoreModel>) => actions.setDialog
	);

	const handleClickOpen = () => {
		setDialog({
			isOpen: true,
			data: item
		});
	};

	return (
		<Grid item xs={12} sm={6} md={4}>
			<Card className={classes.root}>
				<CardHeader
					avatar={
						<Avatar aria-label="recipe" className={classes.avatar}>
							{item.userId}
						</Avatar>
					}
					action={
						<IconButton aria-label="settings"  onClick={handleClickOpen}>
							<MoreVertIcon />
						</IconButton>
					}
					title={item.title}
					subheader="September 14, 2016"
				/>
				<CardMedia
					className={classes.media}
					image="https://cdn.pixabay.com/photo/2021/01/01/21/56/cooking-5880136_1280.jpg"
					title="Paella dish"
				/>
				<CardContent>
					<Typography variant="body2" component="p">
						{item.body}
					</Typography>
				</CardContent>
				<CardActions disableSpacing>
					<IconButton aria-label="add to favorites">
						<FavoriteIcon />
					</IconButton>
					<IconButton
						className={clsx(classes.expand, {
							[classes.expandOpen]: expanded
						})}
						onClick={handleExpandClick}
						aria-expanded={expanded}
						aria-label="show more">
						<ExpandMoreIcon />
					</IconButton>
				</CardActions>
				<Collapse in={expanded} timeout="auto" unmountOnExit>
					<CardContent>
						<Typography paragraph>Method:</Typography>
						<Typography paragraph>
							Heat 1/2 cup of the broth in a pot until simmering, add saffron and
							set aside for 10 minutes.
						</Typography>
						<Typography paragraph>
							Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet
							over medium-high heat. Add chicken, shrimp and chorizo, and cook,
							stirring occasionally until lightly browned, 6 to 8 minutes.
							Transfer shrimp to a large plate and set aside, leaving chicken and
							chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes,
							onion, salt and pepper, and cook, stirring often until thickened and
							fragrant, about 10 minutes. Add saffron broth and remaining 4 1/2
							cups chicken broth; bring to a boil.
						</Typography>
						<Typography paragraph>
							Add rice and stir very gently to distribute. Top with artichokes and
							peppers, and cook without stirring, until most of the liquid is
							absorbed, 15 to 18 minutes. Reduce heat to medium-low, add reserved
							shrimp and mussels, tucking them down into the rice, and cook again
							without stirring, until mussels have opened and rice is just tender,
							5 to 7 minutes more. (Discard any mussels that don’t open.)
						</Typography>
						<Typography>
							Set aside off of the heat to let rest for 10 minutes, and then
							serve.
						</Typography>
					</CardContent>
				</Collapse>
			</Card>
		</Grid>
	);
};
