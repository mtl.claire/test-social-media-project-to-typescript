import React from 'react';
import { Grid, Card, CardActionArea, CardMedia} from '@material-ui/core';
import { PhotoInterface } from "../model/photos";

export const PhotoItem = (item:PhotoInterface) => {
	return (
		<Grid item xs={6} sm={4} md={3}>
			<Card>
				<CardActionArea>
					<CardMedia
					component="img"
					alt={item.url}
					height="140"
					image={item.download_url}
					title="Contemplative Reptile"
					/>
				</CardActionArea>
			</Card>
		</Grid>
	);
}

