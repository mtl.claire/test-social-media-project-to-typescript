import React from "react";
import { useEffect } from "react";
import { Actions } from 'easy-peasy';
import { makeStyles } from "@material-ui/core/styles";
import { IconButton, Typography, Toolbar, Dialog, Button, AppBar } from "@material-ui/core";
import { List, ListItem, ListItemText, TextField } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import { StoreModel } from "../model";
import { useStoreState, useStoreActions } from "../store/hooks";

const useStyles = makeStyles((theme) => ({
	root: {
		margin: "0"
	},
	fabButton: {
		position: 'fixed',
		zIndex: 1,
		bottom: 70,
		right: 20,
		color: "#fff",
		backgroundColor: "#303f9f"
	},
	title: {
		flexGrow: 1,
		textAlign: "center"
	},
	editContainer: {
		padding: "70px 10px"
	},
	textField: {
		width: "calc(100% - 32px)",
		padding: "0 16px"
	},
	appBar: {

	}
}));

type PostValues = {
	title: string;
	body: string;
};

export const PostDialog = () => {
	const classes = useStyles();

	const postDialog = useStoreState((state) => state.postDialog);
	const setDialog = useStoreActions(
		(actions: Actions<StoreModel>) => actions.setDialog
	);

	const postValue = postDialog.data;
	const [values, setValues] = React.useState<PostValues>({
		title: postValue ? postValue.title : "",
		body: postValue ? postValue.body : ""
	});

	useEffect(() => {
		setValues({
			title: postValue ? postValue.title : "",
			body: postValue ? postValue.body : ""
		});
	}, [postValue]);

	const handleChange = (fieldName: keyof PostValues) => (
		e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
	) => {
		setValues({ ...values, [fieldName]: e.currentTarget.value });
	};

	const isEditPost = postValue && postValue.id;
	const dialogTitle = isEditPost ? "Edit Post" : "Add new Post";

	const handleClose = (event: React.MouseEvent<HTMLButtonElement>) => {
		let isSave = event.currentTarget.name === "save";
		let isDelete = event.currentTarget.name === "delete";
		let isUpdatePost = isSave && postValue;
		let isCreatePost = isSave && !postValue;

		const userId = 1; //HardCode for JSONplaceholder query

		if(isUpdatePost) {
			let updateData = {
				...postValue, 
				userId: userId, 
				body: values.body, 
				title: values.title
			}
			updatePostThunk(updateData);
		}

		if(isCreatePost) {
			let newData = {
				userId: userId,
				body: values.body,
				title: values.title
			}
			creatPostThunk(newData);
		}
		
		if(isDelete && postValue) {
			deletePostThunk(postValue);
		}
		
		setDialog({
			isOpen: false,
			data: postValue
		});
	};
	
	const creatPostThunk = useStoreActions(
		(actions: Actions<StoreModel>) => actions.creatPostThunk
	);

	const updatePostThunk = useStoreActions(
		(actions: Actions<StoreModel>) => actions.updatePostThunk
	);

	const deletePostThunk = useStoreActions(
		(actions: Actions<StoreModel>) => actions.deletePostThunk
	);

	return (
		<Dialog fullScreen open={postDialog.isOpen} >
			<AppBar className={classes.appBar}>
				<Toolbar>
					<IconButton name="close" edge="start" color="inherit" onClick={handleClose} aria-label="close">
					<CloseIcon />
					</IconButton>
					<Typography variant="h6" className={classes.title}>
						{dialogTitle}
					</Typography>
					{isEditPost && 
						<Button autoFocus name="delete" variant="contained" color="secondary" onClick={handleClose}>
							Delete
						</Button>
					}
				</Toolbar>
			</AppBar>
			<List className={classes.editContainer}>
				<ListItem>
					<ListItemText primary="Title" />
				</ListItem>
				<TextField
					className={classes.textField}
					autoFocus
					margin="dense"
					value={values.title}
					onChange={handleChange("title")}
				/>
				<ListItem>
					<ListItemText primary="Post"/>
				</ListItem>
				<TextField
					className={classes.textField}
					autoFocus
					margin="dense"
					value={values.body}
					multiline
					onChange={handleChange("body")}
				/>
				<ListItem>
					<Button autoFocus variant="outlined" color="primary" name="save" onClick={handleClose}>
					Save
					</Button>
				</ListItem>
			</List>
		</Dialog>
	)
}
