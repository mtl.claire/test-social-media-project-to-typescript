import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Switch from "@material-ui/core/Switch";
import TranslateIcon from "@material-ui/icons/Translate";
import { Trans } from '@lingui/macro';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { useStoreState, useStoreActions } from "../store/hooks";
import { StoreModel } from "../model";
import { Actions } from 'easy-peasy';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1
	},
	menuButton: {
		marginRight: theme.spacing(2)
	},
	title: {
		flexGrow: 1
	},
	formControl:{

	}
}));

//TODO: get the translate list from lingui
const options = [
	{
	  label: "English",
	  value: "en",
	},
	{
	  label: "中文",
	  value: "zh",
	},
  ];

export const Header = () => {
	const classes = useStyles();
    const currentLanguage = useStoreState((state) => state.currentLanguage);
	const setCurrentLanguage = useStoreActions(
        (actions: Actions<StoreModel>) => actions.setCurrentLanguage
    );

	const handleChange = (
		e: React.ChangeEvent<HTMLSelectElement>
	) => {
		setCurrentLanguage(e.currentTarget.value);
    };

	return (
		<div className={classes.root}>
			<AppBar position="fixed" color="inherit">
				<Toolbar>
					<Typography variant="h6" className={classes.title}>
						LOGO
						<p><Trans>Message Inbox</Trans></p>
					</Typography>
					<Switch color="primary" aria-label="login switch" />
					<FormControl className={classes.formControl}>
						<InputLabel htmlFor="age-native-simple"><TranslateIcon /></InputLabel>
						<select
							id="demo-simple-select"
							value={currentLanguage}
							onChange={handleChange}
							>
							{options.map((option, index) => (
								<option key={index} value={option.value}>{option.label}</option>
							))}
						</select>
					</FormControl>
				</Toolbar>
			</AppBar>
		</div>
	);
};
