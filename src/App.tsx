import React from "react";
import { BrowserRouter, Switch, Route, Link, Redirect } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { Photo as PhotoIcon, InsertComment as InsertCommentIcon, PersonPin as PersonPinIcon } from "@material-ui/icons";
import { Header } from "./components/Header";
import { PostsContainer } from "./pages/Posts";
import { PhotosContainer } from "./pages/Photos";
import { UsersContainer } from "./pages/Users";
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';

import { i18n } from '@lingui/core'
import { I18nProvider } from '@lingui/react'
import { messages as messagesEn } from './locales/en/messages.js';
import { messages as messagesZh } from './locales/zh/messages.js';
import { useStoreState } from "./store/hooks";

const useStyles = makeStyles({
	root: {
		height: "100vh",
		margin: "70px 0"
	},
	toolBar: {
		width: "100%",
		position: "fixed",
		bottom: 0,
		left: 0,
		boxShadow: "0px -1px 20px 0px #d8d8d8"
	},
});

export default function App() {

    //TODO: async the translate file loaded
    i18n.load({
        en: messagesEn,
        zh: messagesZh,
     });

    const currentLanguage = useStoreState((state) => state.currentLanguage);
    i18n.activate(currentLanguage);

	return (
        <I18nProvider i18n={i18n}>
            <div className="App">
                <Header />
                <MainContainer />
            </div>
        </I18nProvider>
	);
}

const MainContainer = () => {
	const classes = useStyles();
	return (
		<BrowserRouter>
			<div className={classes.root}>
				<Switch>
					<Route path="/" exact>
						<PostsContainer />
					</Route>
					<Route path="/photos" exact>
						<PhotosContainer />
					</Route>
					<Route path="/users" exact>
						<UsersContainer />
					</Route>
					<Redirect to="/" />
				</Switch>
				<BottomNav />
			</div>
		</BrowserRouter>
	);
};

const BottomNav = () => {
	const classes = useStyles();
	let defaultLocation = window.location.pathname;
	defaultLocation = !defaultLocation[1] ? "posts" : defaultLocation.substr(1);
	const [value, setValue] = React.useState(defaultLocation);
	const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
		setValue(newValue);
	};
	return (
		<BottomNavigation value={value} onChange={handleChange} className={classes.toolBar}>
			<BottomNavigationAction component={Link} to="/" label="Posts" value="posts" icon={<InsertCommentIcon />} />
			<BottomNavigationAction component={Link} to="/photos" label="Photos" value="photos" icon={<PhotoIcon />} />
			<BottomNavigationAction component={Link} to="/users" label="Friends" value="users" icon={<PersonPinIcon />} />
		</BottomNavigation>
	);
}
