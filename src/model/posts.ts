export interface PostInterface {
	userId: number;
	id?: number;
	title: string;
	body: string;
}

export interface PostDailogInterface {
	isOpen: boolean;
	data: PostInterface | null;
}