import { Action, Thunk } from "easy-peasy";
import { PostInterface, PostDailogInterface } from "./posts"
import { PhotoInterface } from "./photos"
import { UserInterface } from "./users"

export interface StoreModel {
	posts: PostInterface[];
	fetchAllPosts: Thunk<StoreModel>;
	setPosts: Action<StoreModel, PostInterface[]>;
	createPost: Action<StoreModel, PostInterface>;
	updatePost: Action<StoreModel, PostInterface>;
	updatePostThunk: Thunk<StoreModel, PostInterface>;
	creatPostThunk: Thunk<StoreModel, PostInterface>;
	deletePostThunk: Thunk<StoreModel, PostInterface>;

	postDialog: PostDailogInterface;
	setDialog: Action<StoreModel, PostDailogInterface>;

	photos: PhotoInterface[];
	fetchAllPhotos: Thunk<StoreModel>;
	setPhotos: Action<StoreModel, PhotoInterface[]>;

	users: UserInterface[];
	fetchAllUsers: Thunk<StoreModel>;
	setUsers: Action<StoreModel, UserInterface[]>;
	currentLanguage: string;
	setCurrentLanguage: Action<StoreModel, string>;
}
